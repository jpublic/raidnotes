# RAID Shit: So You Want An Encrypted RAID

Instructions for creating, mounting, and fighting with encrypted RAID on Ubuntu.

## Setting Up A New Encrypted Raid

**Step 0**: Installing mdadm & cryptsetup if you haven't already:

```
sudo apt-get update && sudo apt-get install mdadm cryptsetup
```

**Step 1**: Find which disks we will be using:

```
lsblk
```

note which drives you will be using, for me it was `/dev/sdd` and `/dev/sde`


**Step 2**: Creating the raid device:

Decide which raid type you would like to use if you have not already.  The example
below is for a 2 device RAID 1, but you would just change the commands according to devices, number of devices, type of raid, etc:

```
sudo mdadm --create /dev/md420 --chunk=4 --level=1 --raid-devices=2 /dev/sdd /dev/sde
```

Generic Version of this Command:

```
sudo mdadm --create /dev/md420 --chunk=4 --level=<raid type> --raid-devices=<number of devices> <device list, eg. /dev/sdb /dev/sdc etc...>  
```

In my case I used a chunk size of 4 bytes, raid level 1(mirroring), but you can do whatever you want.

**Step 3**:  Creating the cryptFS

Once the RAID is done setting itself up, we will luks encrypt the drive space

```
sudo cryptsetup luksFormat /dev/md0
sudo cryptsetup luksOpen /dev/md0 cryptodrivename # name can be whatever you want
```

**Step 4**:  Creating the filesystem within your LUKS disk:

```
sudo mkfs.ext4 /dev/mapper/cryptodrivename
```

Now all that's left is to mount the newly created disk:

**Step 5**:  Mount the damn thing (optional)

```
sudo mkdir /media/secure
sudo mount /dev/mapper/secure /media/secure
```

Doing this from a fresh boot:

Opening and mounting:

```
sudo cryptsetup luksOpen /dev/md0 cryptodrivename
sudo mount /dev/mapper/cryptodrivename /media/cryptodrivename
```
Unmounting and closing:

```
sudo umount /media/secure
sudo cryptsetup luksClose secure
```

[Source Article](https://matthiaslee.com/encrypted-partition-on-a-soft-raid-device-using-mdadm-and-cryptsetupluks/)


## Mounting At Boot

You probably just want this thing to be permanently available, and not to just manually mount it with a script or commands or whatever.

**Step 0**: Make sure the RAID is registered in mdadm.conf

To get the raid details,

```
sudo mdadm --detail --scan
```

Copy it to mdadm.conf

```
sudo mdadm --detail --scan | sudo tee -a /etc/mdadm/mdadm.conf 
```



**Step 1**: Create a random keyfile.  This will be the computer's way of unlocking it.

``` 
sudo dd if=/dev/urandom of=/root/RAIDkey bs=1024 count=4
```

**Step 2**: Make it root readable only

```
sudo chmod 0400 /root/RAIDkey
```

**Step 3**: Add key as a passphrase to raid

 ```
 sudo cryptsetup luksAddKey /dev/md420 /root/RAIDkey
 ```

**Step 4**: Get the UUID(s) of the device and add them to crypttab and fstab

We will need the UUID to do this right.

```
lsblk -o name,uuid
```

```
sda                    
├─sda1                 F676-3A55
├─sda2                 15ff2795-056c-4e00-8dfb-030420214955
└─sda3                 c566215e-b5eb-448e-ab48-1b085ea606f1
  └─sda3_crypt         hapmsg-YdYY-IcGV-gYUa-XEI5-WBQx-OsVtXT
    ├─vgkubuntu-root   3c27a553-7f55-45ca-ae73-b80fd4cea48f
    └─vgkubuntu-swap_1 bbdcdb9d-a50c-43ed-ab30-20ea668b4e65
sdb                    cc9f17b2-eed2-4ba3-d63d-ed2e017fd02b 
└─md69                 4687a633-89c6-46e2-b2c6-c73f0ed629bf # <-- you want this one
  └─datatopiaverse     eb53d96c-3f55-4462-84b8-a5bec22b68ad
sdc                    cc9f17b2-eed2-4ba3-d63d-ed2e017fd02b 
└─md69                 4687a633-89c6-46e2-b2c6-c73f0ed629bf # <-- you want this one
  └─datatopiaverse     eb53d96c-3f55-4462-84b8-a5bec22b68ad
sdd                    cc9f17b2-eed2-4ba3-d63d-ed2e017fd02b 
└─md69                 4687a633-89c6-46e2-b2c6-c73f0ed629bf # <-- you want this one
  └─datatopiaverse     eb53d96c-3f55-4462-84b8-a5bec22b68ad # <-- use this if you are also mounting in fstab via uuid not mapper

```

**Step 5**: Add the following to crypttab:

**/etc/crypttab**
```
# mapper name
datatopiaverse_crypt UUID=cc9f17b2-eed2-4ba3-d63d-ed2e017fd02b /root/DATATOPIAVERSEkey luks
```

now use the mapper or uuid to add it to fstab

**Step 6**: Add the following to fstab

**/etc/fstab**

by mapper

```
/dev/mapper/datatopiaverse_crypt    /media/datatopiaverse     ext4    defaults         0       2
```

by uuid

```
UUID=eb53d96c-3f55-4462-84b8-a5bec22b68ad      /               ext4    defaults,errors
```
**Step 7**: After changing the /etc/crypttab file, you have to rebuild initramfs:

```
# sudo update-initramfs -u -k all
```

Now Reboot and see if it worked!


[Link To Original Instructions](https://www.howtoforge.com/automatically-unlock-luks-encrypted-drives-with-a-keyfile)

## Troubleshooting: Reassemling, Scanning, etc

To get the raid details,

```
sudo mdadm --detail --scan
```

Copy it to mdadm.conf

```
sudo mdadm --detail --scan | sudo tee -a /etc/mdadm/mdadm.conf 
```

### a) Troubleshooting: Refinding the Raid

If this doesn't work, do the following, replacing dev letters accordingly.  You can use 

```
lsblk
``` 

to find the drive letters of the bum RAID which is fucking off.

```
sudo mdadm --assemble --run --force --update=resync /dev/md69 /dev/sdc /dev/sdd
```


< find a place for this>

sudo mdadm /dev/md420 -a /dev/sdd 

### Other Helpful References On This Subject

- [Creating RAID devices @ linuxgazette.net/](https://linuxgazette.net/140/pfeiffer.html)

- [Manual Setup Encrypted Raid](https://makandracards.com/mookie/2271-manual-setup-raid-encryption-and-lvm-with-linux)

- [Create A Raid @ Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-create-raid-arrays-with-mdadm-on-ubuntu-16-04)

- [Fighting With UUID of RAID, WHICH DO I USE?!](https://unix.stackexchange.com/questions/107810/why-my-encrypted-lvm-volume-luks-device-wont-mount-at-boot-time)

- [Dealing with Raids @ DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-manage-raid-arrays-with-mdadm-on-ubuntu-16-04)

